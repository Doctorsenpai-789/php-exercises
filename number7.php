<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Digit to Word</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
</head>
<body>

<div class="container">
<div class="row justify-content-center " >
<div class="card mt-5">
<div class="card-header bg-warning"><h3>Digit to Word</h3></div>
<div class="card-body bg-danger">
<form method="post">
    <div class="form-group">
    
    <br>
    <h5>Enter a number: </h5>
    <input type="number" name="number"  class="form-control ">
    </div>
    <input type="submit" name="submit" class="btn btn-primary">
    
</form>


<br>
<?php
function getNumber($digit){              
    switch($digit){
        
        case '0':
            echo "Zero ";
            break;
        case '1':
            echo "One ";
            break;
        case '2':
            echo "Two ";
            break;
        case '3':
            echo "Three ";
            break;
        case '4':
            echo "Four ";
            break;
        case '5':
            echo "Five ";
            break;
        case '6':
            echo "Six ";
            break;
        case '7':
            echo "Seven ";
            break;
        case '8':
            echo "Eight ";
            break;
        case '9':
            echo "Nine ";
            break;
                            
    }

}

if(isset($_POST['submit'])){
    $inputNum = $_POST['number'];
    display($inputNum);
}

function display($inputNum){
    $strLength = strlen((string)$inputNum);
    for ($counter=0; $counter <$strLength ; $counter++) { 
        getNumber($inputNum[$counter]);
    }

}


    ?>
     </div>
    </div>
    </div>
    </div>
</body>
</html>