<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Armstrong Number</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
</head>
<body>

<div class="container">
<div class="row justify-content-center " >
<div class="card mt-5">
<div class="card-header bg-success"><h3>JSON DATA</h3></div>
<div class="card-body bg-dark text-white">
<?php

$json_data = '[
    {
    "name" : "John Garg",
    "age"  : "15",
    "school" : "Ahlcon Public school"
    },
    {
    "name" : "Smith Soy",
    "age"  : "16",
    "school" : "St. Marie school"
    },
    {
    "name" : "Charle Rena",
    "age"  : "16",
    "school" : "St. Columba school"
    }
]';


$someArrays = json_decode($json_data, true);


foreach ($someArrays as $key => $value) {
    echo "name :". $value["name"] .", <br>"."age :".$value["age"] . ", <br>" ."school :" .$value["school"] . "<br>";
    echo "<hr>";
}






?>
        
    
    
    
     </div>
    </div>
    </div>
    </div>
</body>
</html>
  